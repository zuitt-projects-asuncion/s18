	/*

ACTIVITY: 

	>> Create a new set of pokemon for pokemon battle.

	>> Solve the health of the pokemon that when tackle is invoked, current value of target’s health should decrease continuously as many times the tackle is invoked
	(target health - this attack)
	
	>> If health is below 10, invoke faint function

*/


	function Pokemon(name, lvl, hp){

		//Properties
		this.name = name;
		this.lvl = lvl;
		this.health = hp * 2;
		this.attack = lvl;

		// methods 
		this.tackle = function(target){
			console.log(`${this.name} tackled ${target.name}`);

			for(i = this.health; i >= 0; i--){
				if(i <= 10){
					console.log(`${this.name} fainted`)//END if (function fainted) 
					break;

				} // END if i lesser than or equal to 10
				if(this.health >= 11){
				//	console.log(this.health = this.health - this.attack);
					console.log(`${this.name}'s health is reduced to ${this.health}`)

				}// END if health is greater than 10	
			} //END for decrement health

			} // END FUNCTION TACKLE

		}; //END FUNCTION POKEMON


	// create new instances of the Pokemon object each with their unique properties
	let pikachu = new Pokemon("Pikachu", 3, 50);
	let bulbasaur = new Pokemon("Bulbasaur", 3, 100);
	let raichu = new Pokemon("Raichu", 5, 101);
	let ratata = new Pokemon("Ratata", 5, 25);
	let squirtle = new Pokemon("Squirtle", 3, 55);

	// Providing the "ratata" object as an argument to "pikachu" tackle method will create interaction between the two objects.

	pikachu.tackle(ratata);
	ratata.tackle(pikachu);
	bulbasaur.tackle(raichu);
	squirtle.tackle(ratata);

