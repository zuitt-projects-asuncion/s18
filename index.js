/*alert("hi");*/

// A variable with multiple values
// a: array
let cartoons = ["Elmo", "Mickey", "Daisy", "Barbie"] 

//Javascript objects
	//similar to an array, it also contains multiple values.
	// unlike an array that uses index, objects use properties.
	//with object, we can easily give labels to each value.

	//Structure or Syntax
		/*
			let objectName = {
				key : value
				key : (function)
				key : object{	
					key : value
				}, 
				key : [array]
			}

		*/

		let cellphone = {
			name: "Nokia 3210", 
			manufactureDate: 1999
		};

		console.log(cellphone);
		console.log(typeof cellphone);

//Creating objects using a constructor function
/* 
	Syntax: 
	function objectName(keyA, keyB){
		this.keyA = keyA;
		this.keyB = keyB;
	}

*/

	function Laptop(name, manufactureDate){
		this.name = name;
		this.releaseDate = manufactureDate;
	};

	let laptop = new Laptop("Lenovo, 2008") //to invoke Laptop, use "new".  It creates a new instance of an object
	console.log(laptop);

	let laptop1 = Laptop("Asus", 2010);
	console.log(laptop1); //result : undefined because the "new" word was not used to define it as an object

// Creating empty objects
	let computer = {};
	let myComputer = new Object();
	console.log(computer);
	console.log(myComputer);

// Accessing Array Objects

let machines = [laptop, laptop1];

//access the property of an object inside an array
console.log(machines[0].name);
//result : Lenovo

//another way of accessing properties in an array
console.log(machines[0]["releaseDate"]);
// result: 2008

// Initializing / Adding /Deleting /Reassigning Object Properties

	let car = {};

	// initializing or adding object properties using dot notation.

	car.name = "Honda Civic";
	console.log(car);

	// adding object properties with square brackets which is basically the same as using dot notation (car.name is the same as car["manufactureDate"])
	car["manufactureDate"] = 2019;
	console.log(car);

// deleting object properties
	delete car["name"];
	console.log(car);

/*
	Mini-Activity
	1. Add properties to the object car:
		a. brand
		b. model
		c. color
		d. location
*/

	car.brand = "Honda"
	car.model = "Civic"
	car.color = "red"
	car.location = "La Union"
	console.log(car);

// Reassigning object properties
	car.manufactureDate = 1986
	console.log(car);

// Object Methods
	// a method is a function which is a property of an object
	// they are also functions and one of the key differences they have is that methods are functions realted to a specific object.
	//methods are defined based on what an object is capable of doing and how it should work.

	let person = {
		name: "Jin",
		//function talk()
		talk :function(){
			console.log(`Hello my name is ${this.name}`)
		}
	};
	person.talk();

	let friend = {
		firstName : "Bam",
		lastName : "Jeon",
		address : {
			city: "Caloocan",
			country: "Philippines"
		},
		emails : ["bam@mail.com", "jambam@gmail.com"],
		introduce : function(){
			console.log(`Hello my name is ${this.firstName} ${this.lastName}, I live in ${this.address.city}. My email is ${this.emails[0]}`)
		} 
	};
	friend.introduce();

	// Real World Application
		// Scenario:
			// 1. Create a game that would have several pokemons to interact with each other.
			// 2. Every pokemon should have stats, properties, functions

	let myPokemon = {
		name : "Bulbasaur",
		level : 3,
		health : 100,
		attack : 50,
		tackle : function(){
			console.log(`${this.name} tackled another pokemon`)
			console.log(`targetPokemon's health is now reduced`)
		},
		faint: function(){
			console.log(`${this.name} fainted`)
		} 
	};
	myPokemon.tackle();
	myPokemon.faint();

	// Creating object with an object constructor

	function Pokemon(name, lvl, hp){

		//Properties
		this.name = name;
		this.lvl = lvl;
		this.health = hp * 2;
		this.attack = lvl;

		// methods 
		this.tackle = function(target){
			console.log(`${this.name} tackled ${target.name}`);
			console.log(`targetPokemon's health is now reduced`);
			console.log(target.name);
			console.log(this.name);
		};
		this.faint = function(){
			console.log(`${this.name} fainted`)
		}
	};

	// create new instances of the Pokemon object each with their unique properties
	let pikachu = new Pokemon("Pikachu", 3, 50);
	let ratata = new Pokemon("Ratata", 5, 25);

	// Providing the "ratata" object as an argument to "pikachu" tackle method will create interaction between the two objects.

	pikachu.tackle(ratata);
	ratata.tackle(pikachu); 
